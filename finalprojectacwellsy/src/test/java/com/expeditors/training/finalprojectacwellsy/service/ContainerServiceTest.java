//package com.expeditors.training.finalprojectacwellsy.service;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.hasProperty;
//import static org.hamcrest.Matchers.is;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//
//import com.expeditors.training.finalprojectacwellsy.config.ServiceTestConfig;
//import com.expeditors.training.finalprojectacwellsy.model.Container;
//import com.expeditors.training.finalprojectacwellsy.model.Container.Status;
//import com.github.springtestdbunit.DbUnitTestExecutionListener;
//import com.github.springtestdbunit.annotation.DatabaseSetup;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes={ServiceTestConfig.class})
//@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
//	DirtiesContextTestExecutionListener.class,
//	TransactionalTestExecutionListener.class,
//	DbUnitTestExecutionListener.class})
//public class ContainerServiceTest {
//	
//	@Autowired
//	public ContainerService containerService;
//	
//	@Test
//	@DatabaseSetup("data.xml")
//	public void testGetContainerById() {
//		Container c = containerService.get(1L);
//		assertThat(c, hasProperty("displayName", is("AAAA1234567")));
//		assertThat(c, hasProperty("capacityAvailable", is(40.)));
//		assertThat(c, hasProperty("currentLocationCode", is("PDX")));
//		assertThat(c, hasProperty("nextLocationCode", is("SEA")));
//		assertThat(c, hasProperty("status", is(Status.READY)));
//	}
//	
//	@Test
//	@DatabaseSetup("data.xml")
//	public void testGetContainerByIdNextLocationNullStatusTRANSIT() {
//		Container c = containerService.get(2L);
//		assertThat(c, hasProperty("displayName", is("DBAG1112222")));
//		assertThat(c, hasProperty("capacityAvailable", is(0.)));
//		assertThat(c, hasProperty("currentLocationCode", is("PDX")));
//		assertThat(c, hasProperty("nextLocationCode", is((String)null)));
//		assertThat(c, hasProperty("status", is(Status.TRANSIT)));
//	}
//	
//	@Test
//	@DatabaseSetup("data.xml")
//	public void testGetContainerByIdStatusARRIVED() {
//		Container c = containerService.get(3L);
//		assertThat(c, hasProperty("displayName", is("MAMA0000999")));
//		assertThat(c, hasProperty("capacityAvailable", is(1.)));
//		assertThat(c, hasProperty("currentLocationCode", is("PDX")));
//		assertThat(c, hasProperty("nextLocationCode", is("SEA")));
//		assertThat(c, hasProperty("status", is(Status.ARRIVED)));
//	}
//	
//}
