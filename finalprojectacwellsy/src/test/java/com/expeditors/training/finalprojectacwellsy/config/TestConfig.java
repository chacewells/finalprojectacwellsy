package com.expeditors.training.finalprojectacwellsy.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.expeditors.training.finalprojectacwellsy.controller.UserController;
import com.expeditors.training.finalprojectacwellsy.service.DetailsServiceImpl;
import com.expeditors.training.finalprojectacwellsy.service.JsService;
import com.expeditors.training.finalprojectacwellsy.service.ShipmentService;
import com.expeditors.training.finalprojectacwellsy.service.UserService;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com.expeditors.training.finalprojectacwellsy.controller")
@Import({PersistenceJPATestConfig.class})
public class TestConfig extends WebMvcConfigurerAdapter {
	@Bean
	public JsService containerService() {
		return Mockito.mock(JsService.class);
	}
	
	@Bean
	public ShipmentService shipmentService() {
		return Mockito.mock(ShipmentService.class);
	}
	
	@Bean
	public UserDetailsService userDetailsService() {
		return Mockito.mock(DetailsServiceImpl.class);
	}
	
	@Bean
	public UserController userController() {
		return Mockito.mock(UserController.class);
	}
	
	@Bean
	public UserService userService() {
		return Mockito.mock(UserService.class);
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = 
				new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/view/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
}
