package com.expeditors.training.finalprojectacwellsy.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages="com.expeditors.training.finalprojectacwellsy.service")
@Import({PersistenceJPATestConfig.class})
public class ServiceTestConfig {
}
