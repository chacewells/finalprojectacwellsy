//package com.expeditors.training.finalprojectacwellsy.controller;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.hamcrest.Matchers.hasProperty;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.expeditors.training.finalprojectacwellsy.config.TestConfig;
//import com.expeditors.training.finalprojectacwellsy.model.Shipment;
//import com.expeditors.training.finalprojectacwellsy.service.ShipmentService;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = {TestConfig.class})
//public class ShipmentControllerTest {
//	
//	private MockMvc mockMvc;
//	
//	@Autowired
//	private ShipmentService shipmentService;
//	
//	@Autowired
//	private WebApplicationContext webApplicationContext;
//	
//	@Before
//	public void setUp() {
//		Mockito.reset(shipmentService);
//		
//		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//	}
//	
////	happy path
//	@Test
//	public void testGetShipment() throws Exception {
//		Shipment first = new Shipment("happy shipment", "PDX", "LAX", 100.);
//		Shipment second = new Shipment("unhappy shipment", "LAX", "PDX", 400.);
//		Shipment third = new Shipment("the other one", "JFK", "LHR", 20.);
//
//		Mockito.when(shipmentService.get(1L)).thenReturn(first);
//		Mockito.when(shipmentService.get(2L)).thenReturn(second);
//		Mockito.when(shipmentService.get(3L)).thenReturn(third);
//		
//		mockMvc.perform(get("/shipment/show.html?id=1"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showShipment.jsp"))
//			.andExpect(model().attributeExists("shipment"))
//			.andExpect(model().attribute("shipment", hasProperty("name", equalTo("happy shipment"))))
//			.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo("PDX"))))
//			.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo("LAX"))))
//			.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(100.))));
//		
//		mockMvc.perform(get("/shipment/show.html?id=2"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showShipment.jsp"))
//			.andExpect(model().attributeExists("shipment"))
//			.andExpect(model().attribute("shipment", hasProperty("name", equalTo("unhappy shipment"))))
//			.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo("LAX"))))
//			.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo("PDX"))))
//			.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(400.))));
//		
//		mockMvc.perform(get("/shipment/show.html?id=3"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showShipment.jsp"))
//			.andExpect(model().attributeExists("shipment"))
//			.andExpect(model().attribute("shipment", hasProperty("name", equalTo("the other one"))))
//			.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo("JFK"))))
//			.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo("LHR"))))
//			.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(20.))));
//	}
//	
////	happy path
//	@Test
//	public void testAddShipment() throws Exception {
//		Shipment ship = new Shipment(1, "hello", "OST", "ORD", 100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isMovedTemporarily())
//				.andExpect(redirectedUrl("show.html?id=0"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasNoErrors("shipment"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(ship.getName()))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddShipmentInvalidName() throws Exception {
//		Shipment ship = new Shipment("", "OST", "ORD", 100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isOk())
//				.andExpect(forwardedUrl("/WEB-INF/view/addShipment.jsp"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasFieldErrors("shipment"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(""))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddShipmentInvalidStartLocation() throws Exception {
//		Shipment ship = new Shipment("some name", "goober", "ORD", 100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isOk())
//				.andExpect(forwardedUrl("/WEB-INF/view/addShipment.jsp"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasFieldErrors("shipment", "startLocation"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(ship.getName()))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddShipmentNoStartLocation() throws Exception {
//		Shipment ship = new Shipment("some name", "", "ORD", 100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isOk())
//				.andExpect(forwardedUrl("/WEB-INF/view/addShipment.jsp"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasFieldErrors("shipment", "startLocation"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(ship.getName()))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddShipmentInvalidEndLocation() throws Exception {
//		Shipment ship = new Shipment("some name", "ORF", "redwing shoes", 100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isOk())
//				.andExpect(forwardedUrl("/WEB-INF/view/addShipment.jsp"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasFieldErrors("shipment", "endLocation"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(ship.getName()))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddShipmentInvalidVolume() throws Exception {
//		Shipment ship = new Shipment("some name", "ORF", "ORD", -100.);
//		
//		mockMvc.perform(
//				post("/shipment/add.html")
//					.param("name", ship.getName())
//					.param("startLocation", ship.getStartLocation())
//					.param("endLocation", ship.getEndLocation())
//					.param("volume", String.valueOf(ship.getVolume()))
//				)
//				.andExpect(status().isOk())
//				.andExpect(forwardedUrl("/WEB-INF/view/addShipment.jsp"))
//				.andExpect(model().attributeExists("shipment"))
//				.andExpect(model().attributeHasFieldErrors("shipment", "volume"))
//				.andExpect(model().attribute("shipment", hasProperty("name", equalTo(ship.getName()))))
//				.andExpect(model().attribute("shipment", hasProperty("startLocation", equalTo(ship.getStartLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("endLocation", equalTo(ship.getEndLocation()))))
//				.andExpect(model().attribute("shipment", hasProperty("volume", equalTo(ship.getVolume()))));
//	}
//	
//}
