//package com.expeditors.training.finalprojectacwellsy.controller;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.hamcrest.Matchers.hasItem;
//import static org.hamcrest.Matchers.hasProperty;
//import static org.hamcrest.Matchers.is;
//import static org.hamcrest.Matchers.lessThan;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.expeditors.training.finalprojectacwellsy.config.TestConfig;
//import com.expeditors.training.finalprojectacwellsy.model.Container;
//import com.expeditors.training.finalprojectacwellsy.model.Container.Status;
//import com.expeditors.training.finalprojectacwellsy.service.ContainerService;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = {TestConfig.class})
//public class ContainerControllerTest {
//	
//	private MockMvc mockMvc;
//	
//	@Autowired
//	private ContainerService containerService;
//	
//	@Autowired
//	private WebApplicationContext webApplicationContext;
//	
//	@Before
//	public void setUp() {
//		Mockito.reset(containerService);
//		
//		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//	}
//	
//	@Test
//	public void testGetContainer() throws Exception {
//		Container first = new Container(1L, "XXXX0123456", 50., "YGW", "SEA", Status.ARRIVED);
//		Container second = new Container(2L, "YYYY1234567", 100., "YEG", "PDX", Status.READY);
//		Container third = new Container(3L, "ZZZZ2345678", 150., "YYW", "ORD", Status.TRANSIT);
//		
//		Mockito.when(containerService.get(1L)).thenReturn(first);
//		
//		mockMvc.perform(get("/container/show.html?id=1"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showContainer.jsp"))
//			.andExpect(model().attributeExists("container"))
//			.andExpect(model().attribute("container", hasProperty("id", equalTo(1L))))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo("XXXX0123456"))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(50.))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo("YGW"))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo("SEA"))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo(Status.ARRIVED))));
//
//		Mockito.when(containerService.get(2L)).thenReturn(second);
//		
//		mockMvc.perform(get("/container/show.html?id=2"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showContainer.jsp"))
//			.andExpect(model().attributeExists("container"))
//			.andExpect(model().attribute("container", hasProperty("id", equalTo(2L))))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo("YYYY1234567"))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(100.))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo("YEG"))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo("PDX"))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo(Status.READY))));
//
//		Mockito.when(containerService.get(3L)).thenReturn(third);
//		
//		mockMvc.perform(get("/container/show.html?id=3"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/showContainer.jsp"))
//			.andExpect(model().attributeExists("container"))
//			.andExpect(model().attribute("container", hasProperty("id", equalTo(3L))))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo("ZZZZ2345678"))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(150.))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo("YYW"))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo("ORD"))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo(Status.TRANSIT))));
//			
//	}
//	
//	@Test
//	public void testGetContainers() throws Exception {
//		Container first = new Container(1L, "XXXX0123456", 50., "YGW", "SEA", Status.ARRIVED);
//		Container second = new Container(2L, "YYYY1234567", 100., "YEG", "PDX", Status.READY);
//		Container third = new Container(3L, "ZZZZ2345678", 150., "YYW", "ORD", Status.TRANSIT);
//		
//		List<Container> containers = Arrays.asList(first, second, third);
//		
//		Mockito.when(containerService.list(0,ContainerController.PAGE_SIZE)).thenReturn(containers);
//		
//		mockMvc.perform(get("/container/list.html"))
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/listContainers.jsp"))
//			.andExpect(model().attributeExists("containers"))
//			.andExpect(model().attribute("containers", hasItem(first)))
//			.andExpect(model().attribute("containers", hasItem(second)))
//			.andExpect(model().attribute("containers", hasItem(third)));
//	}
//	
////	happy path
//	@Test
//	public void testAddContainer() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().is(302))
//			.andExpect(redirectedUrl("show.html?id=0"))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerNoDisplayName() throws Exception {
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "displayName"))
//			.andExpect(model().attribute("container", hasProperty("displayName", is((String)null))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerBadDisplayName() throws Exception {
//		String displayName = "fourty-two";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "displayName"))
//			.andExpect(model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerOverCapacity() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = -.1;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "capacityAvailable"))
//			.andExpect(model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(lessThan(0.)))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerBarelyOverCapacity() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = -.1;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "capacityAvailable"))
//			.andExpect(model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(lessThan(0.)))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerInvalidCapacity() throws Exception {
//		String displayName = "XXXX1234567";
//		String capacityAvailable = "this is the capacity field";
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", capacityAvailable)
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "capacityAvailable"))
//			.andExpect(model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(0.))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerInvalidCurrentLocation() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "the sleuth Bluth";
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "currentLocationCode"))
//			.andExpect( model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerMissingCurrentLocation() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String nextLocationCode = "SEA";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "currentLocationCode"))
//			.andExpect( model().attribute("container", hasProperty("displayName", is(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is((String)null))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerInvalidNextLocation() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "privacy";
//		Status status = Status.READY;
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", String.valueOf(status))
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "nextLocationCode"))
//			.andExpect( model().attribute( "container", hasProperty( "displayName", is(displayName) ) ) )
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", is(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", is(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", is(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", is(status))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerMissingStatus() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "status"))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo((Status)null))));
//	}
//	
////	crappy path
//	@Test
//	public void testAddContainerInvalidStatus() throws Exception {
//		String displayName = "XXXX1234567";
//		double capacityAvailable = 100.;
//		String currentLocationCode = "PDX";
//		String nextLocationCode = "SEA";
//		String status = "foo";
//		
//		mockMvc.perform(post("/container/add.html")
//			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
//			.param("displayName", displayName)
//			.param("capacityAvailable", String.valueOf(capacityAvailable))
//			.param("currentLocationCode", currentLocationCode)
//			.param("nextLocationCode", nextLocationCode)
//			.param("status", status)
//		)
//			.andExpect(status().isOk())
//			.andExpect(forwardedUrl("/WEB-INF/view/addContainer.jsp"))
//			.andExpect(model().attributeHasFieldErrors("container", "status"))
//			.andExpect(model().attribute("container", hasProperty("displayName", equalTo(displayName))))
//			.andExpect(model().attribute("container", hasProperty("capacityAvailable", equalTo(capacityAvailable))))
//			.andExpect(model().attribute("container", hasProperty("currentLocationCode", equalTo(currentLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("nextLocationCode", equalTo(nextLocationCode))))
//			.andExpect(model().attribute("container", hasProperty("status", equalTo((Status)null))));
//	}
//}
