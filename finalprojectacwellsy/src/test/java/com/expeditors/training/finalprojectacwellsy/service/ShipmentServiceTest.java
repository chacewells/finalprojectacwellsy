//package com.expeditors.training.finalprojectacwellsy.service;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.hasProperty;
//import static org.hamcrest.Matchers.is;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestExecutionListeners;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
//import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
//import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
//
//import com.expeditors.training.finalprojectacwellsy.config.ServiceTestConfig;
//import com.expeditors.training.finalprojectacwellsy.model.Shipment;
//import com.github.springtestdbunit.DbUnitTestExecutionListener;
//import com.github.springtestdbunit.annotation.DatabaseSetup;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes={ServiceTestConfig.class})
//@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
//	DirtiesContextTestExecutionListener.class,
//	TransactionalTestExecutionListener.class,
//	DbUnitTestExecutionListener.class})
//public class ShipmentServiceTest {
//	
//	@Autowired
//	public ShipmentService shipmentService;
//	
//	@Test
//	@DatabaseSetup("data.xml")
//	public void testGetContainerById() {
//		Shipment s = shipmentService.get(1L);
//		assertThat(s, hasProperty("name", is("Dixie Chicks Greatest Fits")));
//		assertThat(s, hasProperty("startLocation", is("PDF")));
//		assertThat(s, hasProperty("endLocation", is("JSP")));
//		assertThat(s, hasProperty("volume", is(220.)));
//	}
//
//}
