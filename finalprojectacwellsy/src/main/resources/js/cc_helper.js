var Status = Java.type("com.expeditors.training.finalprojectacwellsy.model.Container.Status");
var FieldError = Java.type("org.springframework.validation.FieldError");
load('classpath:js/underscore.js');
load('classpath:js/backbone.js');

var Container = Backbone.Model.extend({
	validate: function (attrs, options) {
		var errors = [];
		if ( typeof attrs.displayName === 'string' && !attrs.displayName.match( /[A-Z]{4}\d{7}/ ) ) {
			errors.push({
				field: 'displayName',
				rejectedValue: attrs.displayName,
				bindingFailure: false,
				codes: ['Pattern.container.displayName'],
				arguments: ['org.springframework.context.support.DefaultMessageSourceResolvable'],
				defaultMessage: 'Display name must consist of 4 uppercase characters followed by 7 digits; e.g. "AXBJ0004213".' 
			});
		}
		if ( typeof attrs.capacityAvailable === 'number' && attrs.capacityAvailable < 0 ) {
			errors.push({
				field: 'capacityAvailable',
				rejectedValue: attrs.capacityAvailable,
				bindingFailure: false,
				codes: ['DecimalMin.container.capacityAvailable'],
				arguments: ['org.springframework.context.support.DefaultMessageSourceResolvable'],
				defaultMessage: 'Capacity available must be positive' 
			});
		}
		if ( typeof attrs.currentLocationCode === 'string' && !attrs.currentLocationCode.match( /[A-Z]{3}/ ) ) {
			errors.push({
				field: 'currentLocationCode',
				rejectedValue: attrs.currentLocationCode,
				bindingFailure: false,
				codes: ['Pattern.container.currentLocationCode'],
				arguments: ['org.springframework.context.support.DefaultMessageSourceResolvable'],
				defaultMessage: 'Location must consist of 3 uppercase letters; e.g. "PDX"' 
			});
		}
		if ( typeof attrs.nextLocationCode === 'string' && !attrs.nextLocationCode.match( /([A-Z]{3}|^$)/ ) ) {
			errors.push({
				field: 'nextLocationCode', 
				rejectedValue: attrs.nextLocationCode,
				bindingFailure: false,
				codes: ['Pattern.container.nextLocationCode'],
				arguments: ['org.springframework.context.support.DefaultMessageSourceResolvable'],
				defaultMessage: 'Location must consist of 3 uppercase letters; e.g. "PDX"' 
			});
		}
		if ( !attrs.status || !( attrs.status instanceof Status ) ) {
			errors.push({
				field: 'status',
				rejectedValue: null,
				bindingFailure: true,
				codes: ['NotNull.container.capacityAvailable'],
				arguments: ['org.springframework.context.support.DefaultMessageSourceResolvable'],
				defaultMessage: 'Status must be one of "READY", "TRANSIT", "ARRIVED"' 
			});
		}
		if ( errors.length > 0 ) {
			return errors;
		}
	}
});

function backboneContainer (jContainer) {
	return new Container({
		displayName: jContainer.getDisplayName(),
		capacityAvailable: jContainer.getCapacityAvailable(),
		currentLocationCode: jContainer.getCurrentLocationCode(),
		nextLocationCode: jContainer.getNextLocationCode(),
		status: jContainer.getStatus()
	});
}

function validateJavaContainer (jContainer, bindingResult) {
	var container = backboneContainer(jContainer);
	if ( !container.isValid() ) {
		var errors = container.validate( container.attributes );
		for each (var error in errors) {
			bindingResult.addError(
					new FieldError(
							'container', 
							error.field, 
							error.rejectedValue, 
							error.bindingFailure, 
							error.codes,
							error.arguments,
							error.defaultMessage));
		}
	}
}