var notBlank = Java.type("com.expeditors.training.finalprojectacwellsy.util.ValidationUtil").notBlank;
var Container = Java.type("com.expeditors.training.finalprojectacwellsy.model.Container");
var String = Java.type("java.lang.String");
var Long = Java.type("java.lang.Long");

function transactional (operation) {
	var transaction = entityManager.getTransaction();
	transaction.begin();
	
	var result = operation();
	
	transaction.commit();
	
	return result;
}

function getContainer (id) {
	return entityManager.find(Container.class, id);
}

function listContainers (start, max) {
	return entityManager.createQuery("Select c from Container c", Container.class)
	.setFirstResult(start)
	.setMaxResults(max)
	.getResultList();
}

function allContainers () {
	return entityManager
	.createQuery("Select c from Container c", Container.class)
	.getResultList();
}

function findByCriteria(criteria, start, max) {
	var cb = entityManager.getCriteriaBuilder();
	var cQuery = cb.createQuery(Container.class);
	var cRoot = cQuery.from(Container.class);
	
	if (notBlank(criteria.getCurrentLocationCode())) {
		cQuery.where(cb.equal(
					cb.upper(cRoot.get("currentLocationCode").as(String.class)), 
					criteria.getCurrentLocationCode().toUpperCase()));
	}
	if (notBlank(criteria.getDisplayName())) {
		cQuery.where( cb.like( cb.upper( cRoot.get("displayName").as(
				String.class) ),
				"%" + criteria.getDisplayName().toUpperCase() + "%" ) );
	}
	
	return entityManager
			.createQuery(cQuery)
			.setFirstResult(start)
			.setMaxResults(max)
			.getResultList();
}

function findByName(displayName) {
	return entityManager
	.createQuery("Select c from Container c where upper( trim( both from c.displayName) ) like :displayName", Container.class)
	.setParameter(
			"displayName", 
			"%" + displayName.trim().toUpperCase() + "%")
	.getResultList();
}

function totalResults(criteria) {
	var cb = entityManager.getCriteriaBuilder();
	var q = cb.createQuery(Container.class);
	var c = q.from(Container.class);
	if (notBlank(criteria.getCurrentLocationCode())) {
		q.where(cb.equal(
					cb.upper(c.get("currentLocationCode").as(String.class)), 
					criteria.getCurrentLocationCode().toUpperCase()));
	}
	if (notBlank(criteria.getDisplayName())) {
		q.where( cb.like( cb.upper( c.get("displayName").as( String.class ) ),
				"%" + criteria.getDisplayName().toUpperCase() + "%" ) );
	}
	
	return entityManager
			.createQuery(q)
			.getResultList()
			.size();
}

function totalContainers() {
	return entityManager
	.createQuery("Select count(c) from Container c", Long.class)
	.getSingleResult();
}

function save(container) {
	return transactional(function () {
		var containerExists = false;
		if (container.getId() != null) {
			var query = entityManager.createQuery("Select c from Container c where c.id = :id");
			query.setParameter("id", container.getId());
	//		entity exists if query result isn't empty
			containerExists = query.getSingleResult() != null;
		}
		
		if (containerExists) {
			entityManager.merge(container);
		} else {
			entityManager.persist(container);
		}
		
		return container.getId();
	});
}

function remove(id) {
	return transactional(function () {
		var wasRemoved = false;
		
		var container = getContainer(id);
		if (container) {
			entityManager.remove(container);
			wasRemoved = true;
		}
		
		return wasRemoved;
	});
}