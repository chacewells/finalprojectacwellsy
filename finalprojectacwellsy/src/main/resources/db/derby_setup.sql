DROP TABLE CONTAINER;
DROP TABLE SHIPMENT;
CREATE TABLE CONTAINER
(
CONTAINER_ID             BIGINT NOT NULL CONSTRAINT container_pk PRIMARY KEY
                         GENERATED ALWAYS AS IDENTITY(START WITH 1,INCREMENT BY 1),
DISPLAY_NAME             VARCHAR(11) NOT NULL,
CAPACITY_AVAILABLE       DOUBLE NOT NULL DEFAULT 0.0,
CURRENT_LOCATION_CODE    VARCHAR(3) NOT NULL,
NEXT_LOCATION_CODE       VARCHAR(3),
STATUS                   INTEGER NOT NULL DEFAULT 0
);
CREATE TABLE SHIPMENT
(
SHIPMENT_ID              BIGINT NOT NULL CONSTRAINT shipment_pk PRIMARY KEY
                         GENERATED ALWAYS AS IDENTITY(START WITH 1,INCREMENT BY 1),
NAME                     VARCHAR(30) NOT NULL,
START_LOCATION           VARCHAR(3) NOT NULL,
END_LOCATION             VARCHAR(3) NOT NULL,
VOLUME                   DOUBLE NOT NULL DEFAULT 0.0
);

DROP TABLE USERS;
DROP TABLE USER_ROLES;
CREATE TABLE USERS
(
USER_ID              BIGINT NOT NULL CONSTRAINT users_pk PRIMARY KEY
                     GENERATED ALWAYS AS IDENTITY(START WITH 1,INCREMENT BY 1),
USERNAME             VARCHAR(30) NOT NULL,
PASSWORD             VARCHAR(80) NOT NULL
);
CREATE TABLE USER_ROLES
(
USER_ROLE_ID         BIGINT NOT NULL CONSTRAINT user_pk PRIMARY KEY
                     GENERATED ALWAYS AS IDENTITY(START WITH 1,INCREMENT BY 1),
USER_ROLE            VARCHAR(30) NOT NULL,
USER_ID              BIGINT NOT NULL,
CONSTRAINT fk_user_id FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID)
);

INSERT INTO USERS (USERNAME,PASSWORD)
VALUES
    ('master','master'),
    ('maint','work'),
    ('cust','god');

INSERT INTO USER_ROLES (USER_ROLE,USER_ID)
VALUES
	('ROLE_USER',(SELECT USER_ID FROM USERS WHERE USERNAME = 'master' FETCH FIRST ROW ONLY)),
	('ROLE_CONTENT',(SELECT USER_ID FROM USERS WHERE USERNAME = 'master' FETCH FIRST ROW ONLY)),
	('ROLE_USER',(SELECT USER_ID FROM USERS WHERE USERNAME = 'cust' FETCH FIRST ROW ONLY)),
	('ROLE_CONTENT',(SELECT USER_ID FROM USERS WHERE USERNAME = 'maint' FETCH FIRST ROW ONLY));

CREATE VIEW USERS_WITH_ROLES AS
SELECT USERNAME,PASSWORD,USER_ROLE FROM USERS
NATURAL JOIN USER_ROLES;
