package com.expeditors.training.finalprojectacwellsy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTAINER_SHIPMENT")
public class ContainerShipment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CONTAINER_SHIPMENT_ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTAINER_ID", nullable=false)
	private Container container;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SHIPMENT_ID", nullable=false)
	private Shipment shipment;
	
	@Column(name="VOLUME_ASSIGNED")
	private Double volumeAssigned;

	public ContainerShipment() {
		super();
	}

	public ContainerShipment(Container container, Shipment shipment,
			Double volumeAssigned) {
		super();
		this.container = container;
		this.shipment = shipment;
		this.volumeAssigned = volumeAssigned;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}

	public Double getVolumeAssigned() {
		return volumeAssigned;
	}

	public void setVolumeAssigned(Double volumeAssigned) {
		this.volumeAssigned = volumeAssigned;
	}

	public Long getId() {
		return id;
	}
	
}
