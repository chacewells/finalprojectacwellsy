package com.expeditors.training.finalprojectacwellsy.util;

import com.expeditors.training.finalprojectacwellsy.model.Container;

public class ValidationUtil {
	
	public static boolean notBlank(String str) {
		return str != null && !str.isEmpty();
	}
	
	public static boolean notBlank(Container.Status status) {
		return status != null;
	}
	
	public static boolean notBlank(Double dubb) {
		return dubb != null;
	}
	
}
