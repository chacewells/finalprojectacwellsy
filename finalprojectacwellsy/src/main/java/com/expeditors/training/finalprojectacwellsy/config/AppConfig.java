package com.expeditors.training.finalprojectacwellsy.config;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.Function;

import javax.persistence.EntityManagerFactory;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.expeditors.training.finalprojectacwellsy.controller.CCHelper;
import com.expeditors.training.finalprojectacwellsy.service.ContainerService;
import com.expeditors.training.finalprojectacwellsy.service.JsService;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com.expeditors.training.finalprojectacwellsy")
@Import({SecurityConfig.class, PersistenceJPAConfig.class})
public class AppConfig extends WebMvcConfigurerAdapter {
	
	private Function<String,Invocable> jsServiceBuilder = new Function<String, Invocable>() {
		ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("javascript");
		public Invocable apply(String scriptName) {
			try (Reader reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream("js/" + scriptName))) {
				nashorn.put("entityManager", emf.createEntityManager());
				nashorn.eval(reader);
			} catch (ScriptException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return (Invocable)nashorn;
		}
	};
	
	@Autowired
	EntityManagerFactory emf;
	
	@Bean
	InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/view/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
	@Bean
	MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}
	
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
	}
	
	@Bean
	public OpenEntityManagerInViewInterceptor openEntityManagerInView() {
		OpenEntityManagerInViewInterceptor i = new OpenEntityManagerInViewInterceptor();
		i.setEntityManagerFactory(emf);
		return i;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addWebRequestInterceptor(openEntityManagerInView());
	}
	
	@Bean
	public Invocable containerServiceInvocable() {
		return jsServiceBuilder.apply("container_service.js");
	}
	
	@Bean(name="jsContainerService")
	@Autowired
	@Qualifier("containerServiceInvocable")
	public JsService containerService(Invocable jsServiceInvocable) {
		return new JsService(jsServiceInvocable);
	}
	
	@Bean(name="containerService")
	@Autowired
	@Qualifier("containerServiceInvocable")
	public ContainerService containerServiceInterface(Invocable jsServiceInvocable) {
		return jsServiceInvocable.getInterface(ContainerService.class);
	}
	
	@Bean
	public CCHelper containerControllerHelper() {
		ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");
		CCHelper helper = null;
		
		try (Reader reader = new InputStreamReader(getClass().getClassLoader().getResourceAsStream("js/cc_helper.js"))) {
			nashorn.eval(reader);
			helper = ((Invocable)nashorn).getInterface(CCHelper.class);
		} catch (IOException|ScriptException e) {
			throw new RuntimeException(e);
		}
		
		return helper;
	}
	
}
