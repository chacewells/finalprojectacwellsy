package com.expeditors.training.finalprojectacwellsy.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.expeditors.training.finalprojectacwellsy.model.Container;
import com.expeditors.training.finalprojectacwellsy.service.JsService;

@Component
@Path("/containers")
public class ContainerListResource {
	
	@Autowired
	private JsService containerService;
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response listAll() {
		List<Container> containers = containerService.invoke("allContainers");
		GenericEntity<List<Container>> entity = new GenericEntity<List<Container>>(containers){};
		if (!containers.isEmpty()) {
			return Response.ok(entity).build();
		} else {
			return Response.status(204).build();
		}
	}
	
	@GET
	@Path("/name/{name}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response findByName(@PathParam("name") String name) {
		List<Container> containers = containerService.invoke("findByName", name);
		GenericEntity<List<Container>> entity = new GenericEntity<List<Container>>(containers){};
		if (!containers.isEmpty()) {
			return Response.ok(entity).build();
		} else {
			return Response.status(204).build();
		}
	}

}
