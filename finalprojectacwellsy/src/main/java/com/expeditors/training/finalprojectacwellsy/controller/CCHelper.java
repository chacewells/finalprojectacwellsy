package com.expeditors.training.finalprojectacwellsy.controller;

import org.springframework.validation.BindingResult;

import com.expeditors.training.finalprojectacwellsy.model.Container;

public interface CCHelper {
	public void validateJavaContainer(Container jContainer, BindingResult bindingResult);
}
