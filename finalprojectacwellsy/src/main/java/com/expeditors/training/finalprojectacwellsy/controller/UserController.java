package com.expeditors.training.finalprojectacwellsy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.expeditors.training.finalprojectacwellsy.service.UserService;

@Controller
@RequestMapping("/user/")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("list.html")
	public ModelAndView listUsers() {
		return new ModelAndView("listUsers")
		.addObject("users", userService.listAll());
	}
	
}
