package com.expeditors.training.finalprojectacwellsy.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	UserDetailsService userService;
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(userService)
			.passwordEncoder(new StandardPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/static/**").permitAll()
				.antMatchers("/login/**").permitAll()
				.antMatchers("/shipment/**").hasRole("USER")
				.antMatchers("/container/**").hasRole("CONTENT")
				.antMatchers("/rest/**").hasRole("CONTENT")
				.antMatchers("/").authenticated()
				.anyRequest().authenticated()
			.and()
				.formLogin()
			.and()
				.httpBasic()
			.and()
				.csrf().disable().antMatcher("/rest/**");
	}

}
