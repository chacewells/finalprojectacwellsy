package com.expeditors.training.finalprojectacwellsy.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class DbScripter {
	static ScriptEngineManager manager = new ScriptEngineManager();
	static ScriptEngine nashorn = manager.getEngineByName("nashorn");
	static final ClassLoader loader = DbScripter.class.getClassLoader();
	
	public static void main(final String[] args) throws Exception {
		nashorn.put("loader", loader);
		loadSystemProperties();
		loadDriver();
		nashorn.put("template", 
				new NamedParameterJdbcTemplate(
						new SingleConnectionDataSource(
								DriverManager.getConnection(
										System.getProperty("db.url"), 
										System.getProperty("db.username"),
										System.getProperty("db.password")),
										true)));
		
		List<String> scripts = new Supplier<List<String>>() {
			public List<String> get() {
				for (int i = 0; i < args.length; ++i) {
					args[i] = "js/" + args[i];
				}
				return Collections.unmodifiableList(Arrays.asList(args));
			}
		}.get();
		
		for (String script : scripts) {
			try (Reader scriptReader = new InputStreamReader(loader.getResourceAsStream(script))) {
				nashorn.eval(scriptReader);
			}
		}
	}
	
	private static void loadSystemProperties() throws IOException, ScriptException {
		try (Reader sysPropsScript = new InputStreamReader(loader.getResourceAsStream("js/load_system_properties.js"))) {
			nashorn.eval(sysPropsScript);
		}
	}
	
	private static void loadDriver() throws ClassNotFoundException {
		loader.loadClass(System.getProperty("db.driver"));
	}

}
