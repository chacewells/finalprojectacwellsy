package com.expeditors.training.finalprojectacwellsy.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.expeditors.training.finalprojectacwellsy.model.Container;
import com.expeditors.training.finalprojectacwellsy.model.Shipment;
import com.expeditors.training.finalprojectacwellsy.service.ShipmentService;

@Controller
@RequestMapping("/shipment/")
public class ShipmentController {
	
	@Autowired
	private ShipmentService shipmentService;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(method=RequestMethod.GET, value={"add.html", "edit.html"})
	public ModelAndView showAddShipment(@RequestParam(value="id", required=false) Long shipmentId) {
		ModelAndView mav = new ModelAndView();
		
		Shipment shipment = null;
		if (shipmentId != null) {
			shipment = shipmentService.get(shipmentId);
		}
		if (shipment == null) {
			shipment = new Shipment();
		}
		mav.addObject("shipment", shipment);
		
		if (shipmentService.isShipmentEditable(shipmentId)) {
			mav.setViewName("addShipment");
		} else {
			mav.setView(new RedirectView("cant_edit.html"));
			mav.addObject("id", shipmentId);
		}
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value={"add.html", "edit.html"})
	public ModelAndView addShipment(@ModelAttribute("shipment") @Valid Shipment shipment, BindingResult result, Model m) {
		ModelAndView mav = new ModelAndView();
		if (!shipmentService.isShipmentEditable(shipment.getId())) {
			mav.setViewName("cannotEditShipment");
		} else if (result.hasErrors()) {
			mav.setViewName("addShipment");
		} else {
			shipmentService.add(shipment);
			mav.setView(new RedirectView("show.html"));
			mav.addObject("id", shipment.getId());
		}
		
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="show.html")
	public ModelAndView showGetShipment(@RequestParam("id") long id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("showShipment");
		Shipment shipment = shipmentService.get(id);
		for (Container c : shipment.getAssignedContainers()) {
			logger.info("Container fetched: " + c.getDisplayName());
		}
		mav.addObject("shipment", shipment);
		return mav;
	}

	@RequestMapping(method=RequestMethod.GET, value="list.html")
	public ModelAndView listShipments() {
		ModelAndView mav = new ModelAndView("listShipments");
		return mav.addObject("shipments", shipmentService.all());
	}
	
	@RequestMapping(method=RequestMethod.GET, value="cant_edit.html")
	public ModelAndView youCantDoThat(@RequestParam(value="id", required=false) Long shipmentId) {
		Shipment shipment = shipmentId != null ? shipmentService.get(shipmentId) : null;
		return new ModelAndView("cannotEditShipment")
			.addObject("shipment", shipment != null ? shipment : new Shipment());
	}
	
}
