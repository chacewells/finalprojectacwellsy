package com.expeditors.training.finalprojectacwellsy.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="SHIPMENT")
@NamedQueries({
	@NamedQuery(
			name="shipmentsForUsername",
			query="Select s from Shipment s where s.user.username = :username"),
	@NamedQuery(
			name="usernameForShipmentId",
			query="Select s.user.username from Shipment s where s.id = :shipmentId"),
	@NamedQuery(
			name="userForUsername",
			query="Select u from User u where u.username = :username")
})
public class Shipment {
	@Id
	@Column(name="SHIPMENT_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="NAME")
	@NotBlank
	private String name;
	
	@Column(name="START_LOCATION")
	@Pattern(regexp="[A-Z]{3}")
	@NotNull
	private String startLocation;
	
	@Column(name="END_LOCATION")
	@Pattern(regexp="[A-Z]{3}")
	@NotNull
	private String endLocation;
	
	@Column(name="VOLUME")
	@DecimalMin("0")
	private double volume;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USER_ID")
	private User user;
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name="CONTAINER_SHIPMENT",
			joinColumns={@JoinColumn(name="SHIPMENT_ID", nullable=false, updatable=false)},
			inverseJoinColumns={@JoinColumn(name="CONTAINER_ID", nullable=false, updatable=false)})
	private Set<Container> assignedContainers;
	
	public Shipment() {}
	public Shipment(long id, String name, String startLocation,
			String endLocation, double volume) {
		super();
		this.id = id;
		this.name = name;
		this.startLocation = startLocation;
		this.endLocation = endLocation;
		this.volume = volume;
	}
	public Shipment(String name, String startLocation, String endLocation,
			double volume) {
		super();
		this.name = name;
		this.startLocation = startLocation;
		this.endLocation = endLocation;
		this.volume = volume;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public String getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public Set<Container> getAssignedContainers() {
		return assignedContainers;
	}
	public void setAssignedContainers(Collection<Container> containers) {
		if (assignedContainers == null) {
			assignedContainers = new HashSet<Container>();
		}
		assignedContainers.addAll(containers);
	}
	public boolean addContainer(Container container) {
		return assignedContainers.add(container);
	}
	public String getUsername() {
		return user.getUsername();
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isEditable() {
		if (assignedContainers == null || assignedContainers.isEmpty()) {
			return true;
		}
		for (Container c : assignedContainers) {
			if (!c.getStatus().equals(Container.Status.READY)) {
				return false;
			}
		}
		
		return true;
	}
	
//	convenience method for JSP EL
	public boolean getIsEditable() {
		return isEditable();
	}
}
