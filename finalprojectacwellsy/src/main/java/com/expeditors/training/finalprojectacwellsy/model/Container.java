package com.expeditors.training.finalprojectacwellsy.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="CONTAINER")
@XmlRootElement(name="containers")
public class Container {
	public static enum Status {READY, TRANSIT, ARRIVED}
	
	@Id
	@Column(name="CONTAINER_ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotNull
//	@Pattern(regexp = "[A-Z]{4}\\d{7}")
	@Column(name="DISPLAY_NAME")
	private String displayName;
	
//	@DecimalMin(value="0")
	@Column(name="CAPACITY_AVAILABLE")
	private double capacityAvailable;
	
	@NotNull
//	@Pattern(regexp="[A-Z]{3}")
	@Column(name="CURRENT_LOCATION_CODE")
	private String currentLocationCode;
	
//	@Pattern(regexp = "([A-Z]{3}|)")
	@Column(name="NEXT_LOCATION_CODE")
	private String nextLocationCode;
	
//	@NotNull
	@Column(name="STATUS")
	private Status status;
	
	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="assignedContainers")
	private Set<Shipment> assignedShipments;
	
	public Container() {}
	public Container(Long id, String displayName, double capacityAvailable,
			String currentLocationCode, String nextLocationCode, Status status) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.capacityAvailable = capacityAvailable;
		this.currentLocationCode = currentLocationCode;
		this.nextLocationCode = nextLocationCode;
		this.status = status;
	}
	public Container(String displayName, double capacityAvailable,
			String currentLocationCode, String nextLocationCode, Status status) {
		super();
		this.displayName = displayName;
		this.capacityAvailable = capacityAvailable;
		this.currentLocationCode = currentLocationCode;
		this.nextLocationCode = nextLocationCode;
		this.status = status;
	}
	public Container(String displayName, double capacityAvailable,
			String currentLocationCode, String nextLocationCode) {
		super();
		this.displayName = displayName;
		this.capacityAvailable = capacityAvailable;
		this.currentLocationCode = currentLocationCode;
		this.nextLocationCode = nextLocationCode;
		status = Status.READY;
	}
	public Container(Long id, String displayName, double capacityAvailable,
			String currentLocationCode, String nextLocationCode) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.capacityAvailable = capacityAvailable;
		this.currentLocationCode = currentLocationCode;
		this.nextLocationCode = nextLocationCode;
		status = Status.READY;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public double getCapacityAvailable() {
		return capacityAvailable;
	}
	public void setCapacityAvailable(double capacityAvailable) {
		this.capacityAvailable = capacityAvailable;
	}
	public String getCurrentLocationCode() {
		return currentLocationCode;
	}
	public void setCurrentLocationCode(String currentLocationCode) {
		this.currentLocationCode = currentLocationCode;
	}
	public String getNextLocationCode() {
		return nextLocationCode;
	}
	public void setNextLocationCode(String nextLocationCode) {
		this.nextLocationCode = nextLocationCode;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Set<Shipment> getAssignedShipments() {
		return assignedShipments;
	}
	public boolean addShipment(Shipment shipment) {
		return assignedShipments.add(shipment);
	}
	public boolean removeShipment(Shipment shipment) {
		return assignedShipments.remove(shipment);
	}
	public String toJson() {
		String json = "{\"id\":\"" + id +
				"\",\"displayName\":\"" + displayName +
				"\",\"capacityAvailable\":\"" + capacityAvailable +
				"\",\"currentLocationCode\":\"" + currentLocationCode +
				"\",\"nextLocationCode\":\"" + (nextLocationCode != null ? nextLocationCode : "") +
				"\",\"status\":\"" + status + "\"}";
		return json;
	}
	
	@Override
	public String toString() {
		return "Container [id=" + id + ", displayName=" + displayName
				+ ", capacityAvailable=" + capacityAvailable
				+ ", currentLocationCode=" + currentLocationCode
				+ ", nextLocationCode=" + nextLocationCode + ", status="
				+ status + ", assignedShipments=" + assignedShipments + "]";
	}
	
	public void decreaseCapacity(final Number volume) {
		if (volume == null) {
			return;
		}
		if (volume.doubleValue() > capacityAvailable) {
			capacityAvailable = 0.;
		} else {
			capacityAvailable -= volume.doubleValue();
		}
	}
	
	public void increaseCapacity(Number volume) {
		if (volume == null) {
			return;
		}
		capacityAvailable = capacityAvailable + volume.doubleValue();
	}
	
	public boolean hasNextLocationCode() {
		return nextLocationCode != null && !nextLocationCode.trim().isEmpty();
	}
}
