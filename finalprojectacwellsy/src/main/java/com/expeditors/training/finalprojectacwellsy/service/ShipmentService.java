package com.expeditors.training.finalprojectacwellsy.service;

import static com.expeditors.training.finalprojectacwellsy.util.ValidationUtil.notBlank;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.expeditors.training.finalprojectacwellsy.dto.FindContainerCriteria;
import com.expeditors.training.finalprojectacwellsy.model.Container;
import com.expeditors.training.finalprojectacwellsy.model.ContainerShipment;
import com.expeditors.training.finalprojectacwellsy.model.Shipment;
import com.expeditors.training.finalprojectacwellsy.model.User;

@Repository
public class ShipmentService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	public Shipment get(Long id) {
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		Shipment shipment = entityManager.find(Shipment.class, id);
		if ( shipment != null && !principal.getName().equals(shipment.getUsername()) ) {
			throw new IllegalArgumentException("Access denied for user '" + principal.getName() + "'");
		}
		
		return shipment;
	}
	
//	TODO: update to add paging
	public List<Shipment> all() {
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		TypedQuery<Shipment> sQuery =
				entityManager
				.createNamedQuery(
						"shipmentsForUsername", 
						Shipment.class);
		sQuery.setParameter("username", principal.getName());
		return sQuery.getResultList();
	}
	
	@Transactional
	public Long add(Shipment shipment) {
		Principal principal = SecurityContextHolder.getContext().getAuthentication();
		User user = entityManager.createNamedQuery(
				"userForUsername",
				User.class)
				.setParameter("username", principal.getName())
				.getSingleResult();
		if (shipment.getId() == null) {
			shipment.setUser(user);
			entityManager.persist(shipment);
		} else {
			if (!isSameUserForShipment(principal, shipment)) {
				throw new IllegalArgumentException("User not authorized for this action");
			}
			refreshContainers(shipment);
			shipment.setUser(user);
			entityManager.merge(shipment);
			logger.info("yet another analysis of the container set size: " + shipment.getAssignedContainers().size());
		}
		dispatchShipment(shipment.getId());
		return shipment.getId();
	}
	
	private void refreshContainers(Shipment shipment) {
		Shipment existing = get(shipment.getId());
		logger.info("existing container set size: " + existing.getAssignedContainers().size());
		shipment.setAssignedContainers(existing.getAssignedContainers());
		logger.info("new container set size: " + shipment.getAssignedContainers().size());
		entityManager.detach(existing);
	}
	
	private boolean isSameUserForShipment(Principal principal, Shipment shipment) {
		String sUsername = entityManager.createNamedQuery(
				"usernameForShipmentId",
				String.class)
				.setParameter("shipmentId", shipment.getId())
				.getSingleResult();
		return principal.getName().equals(sUsername);
	}
	
	/**
	 * Assigns the full shipment load of the corresponding shipment to
	 * to the corresponding container
	 * @param shipmentId the id of the shipment to be dispatched
	 * @param containerId the id of the container to carry the shipment
	 * @return the id of the ShipmentContainer object created from this transaction
	 */
	@Transactional
	public Long associateFullShipmentToContainer(
			final Shipment shipment,
			final Container container) {
		Double volumeAssigned = shipment.getVolume();
		
		ContainerShipment containerShipment =
				new ContainerShipment(
						container, 
						shipment, 
						volumeAssigned);
		container.decreaseCapacity(volumeAssigned);
		entityManager.merge(container);
		entityManager.persist(containerShipment);
		
		return containerShipment.getId();
	}
	
	@Transactional
	public Long associatePartialShipmentToContainer(
			final Shipment shipment,
			final Container container) {
		Double volumeAssigned = container.getCapacityAvailable();
		
		ContainerShipment containerShipment =
				new ContainerShipment(
						container,
						shipment, 
						volumeAssigned);
		container.setCapacityAvailable(0.);
		entityManager.merge(container);
		entityManager.persist(containerShipment);
		
		return containerShipment.getId();
	}
	
	@Transactional
	public Long associateVolumeOfShipmentToContainer(
			final Double volumeAssigned,
			final Shipment shipment,
			final Container container) {
		
		ContainerShipment containerShipment =
				new ContainerShipment(
						container,
						shipment,
						volumeAssigned);
		container.decreaseCapacity(volumeAssigned);
		entityManager.merge(container);
		entityManager.persist(containerShipment);
		
		return containerShipment.getId();
	}
	/**
	 * Dispatches shipment to container(s)
	 * @param shipmentId id for Shipment object to be dispatched
	 */
	@Transactional
	public void dispatchShipment(Long shipmentId) {
		logger.info("running dispatchShipment");
		dissociateAllContainers(shipmentId);
		Shipment shipment = get(shipmentId);
		
		/*
		 * initial criteria:
		 * 	sufficient capacity
		 * 	current location same as start location
		 * 	next location same as end location
		 * 	status is READY
		 */
		FindContainerCriteria criteria = new FindContainerCriteria();
		criteria.setCurrentLocationCode(shipment.getStartLocation());
		criteria.setNextLocationCode(shipment.getEndLocation());
		criteria.setMinCapacityAvailable(shipment.getVolume());
		criteria.setStatus(Container.Status.READY);
		
//		if matches found, dispatch full shipment to first container
		List<Container> matches = findContainersToAssociate(criteria);
		if (matches.size() > 0) {
			logger.info("dispatchShipment: dispatching full shipment to container");
			associateFullShipmentToContainer(shipment, matches.get(0));
			return;
		}
		
		/*
		 * secondary criteria
		 * 	remove destination requirement
		 */
		criteria.setNextLocationCode(null);
		matches = findContainersToAssociate(criteria);
//		in this case, the container's nextLocation property is updated
//		to match the shipment's endLocation
		if (matches.size() > 0) {
			associateShipmentAndAssignContainerLocation(shipment, matches.get(0));
			return;
		}
		
		/*
		 * tertiary criteria
		 * 	volume requirement is removed so shipment can fill multiple containers
		 */
		criteria.setMinCapacityAvailable(null);
		matches = findContainersToAssociate(criteria);
		if (matches.size() > 0) {
			associateDistributedShipmentToContainers(shipment, matches);
			return;
		}
		
	}
	
	private void associateShipmentAndAssignContainerLocation(Shipment shipment, Container container) {
		if ( !container.hasNextLocationCode() ) {
			container.setNextLocationCode(shipment.getEndLocation());
		}
		container.setNextLocationCode(shipment.getEndLocation());
		associateFullShipmentToContainer(shipment, container);
	}
	
	private void associateDistributedShipmentToContainers(Shipment shipment, List<Container> matches) {
		Double remainingAssignable = shipment.getVolume();
		for (Container container : matches) {
			if ( !container.hasNextLocationCode() ) {
				container.setNextLocationCode(shipment.getEndLocation());
			}
			if (remainingAssignable <= 0.) {break;}
			if (remainingAssignable <= container.getCapacityAvailable()) {
				associateVolumeOfShipmentToContainer(
						remainingAssignable,
						shipment, 
						container);
				remainingAssignable = 0.;
			} else {
				associatePartialShipmentToContainer(shipment, container);
				remainingAssignable -= container.getCapacityAvailable();
			}
		}
	}
	
	@Transactional
	private void dissociateAllContainers(Long shipmentId) {
		logger.info("dissociating containers for shipment id " + shipmentId);
		Collection<ContainerShipment> containerShipments =
				entityManager
				.createQuery(
						"SELECT cs FROM ContainerShipment cs WHERE cs.shipment.id = :shipmentId", 
						ContainerShipment.class)
				.setParameter("shipmentId", shipmentId)
				.getResultList();
		logger.info("number of ContainerShipments: " + containerShipments.size());
		for (ContainerShipment cs : containerShipments) {
			Container container = cs.getContainer();
			logger.info("before container gets capacity back: " + container.getCapacityAvailable());
			container.increaseCapacity(cs.getVolumeAssigned());
			logger.info("after container gets capacity back: " + container.getCapacityAvailable());
			entityManager.remove(cs);
			entityManager.merge(container);
			logger.info("after container merged: " + entityManager.find(Container.class, container.getId()).getCapacityAvailable());
		}
	}
	
	public List<Container> findContainersToAssociate(FindContainerCriteria criteria) {
		String sql = "Select c from Container c ";
		String displayName = criteria.getDisplayName();
		Double minCapacityAvailable = criteria.getMinCapacityAvailable();
		String currentLocationCode = criteria.getCurrentLocationCode();
		String nextLocationCode = criteria.getNextLocationCode();
		Container.Status status = criteria.getStatus();
		
		if (notBlank(displayName)) {
			logger.info("CRITERIA: display name not blank: " + displayName);
			sql += "where c.displayName = :displayName ";
		}
		if (notBlank(minCapacityAvailable)) {
			logger.info("CRITERIA: capacity available not blank: " + minCapacityAvailable);
			sql += (notBlank(displayName) ? "and" : "where") + " c.capacityAvailable >= :capacityAvailable ";
		}
		if (notBlank(currentLocationCode)) {
			logger.info("CRITERIA: current location not blank: " + currentLocationCode);
			sql += (notBlank(displayName) || notBlank(minCapacityAvailable) ? "and" : "where") 
					+ " c.currentLocationCode = :currentLocationCode ";
		}
		if (notBlank(nextLocationCode)) {
			logger.info("CRITERIA: next location not blank: " + nextLocationCode);
			sql += (notBlank(displayName) || notBlank(minCapacityAvailable) || notBlank(currentLocationCode) ? "and" : "where")
					+ " c.nextLocationCode = :nextLocationCode ";
		}
		if (notBlank(status)) {
			logger.info("CRITERIA: status not blank: " + status);
			sql += (notBlank(displayName) || notBlank(minCapacityAvailable) || notBlank(currentLocationCode) || notBlank(nextLocationCode) ? "and" : "where")
					+ " c.status = :status";
		}
		
		TypedQuery<Container> query = entityManager.createQuery(sql, Container.class);

		if (notBlank(displayName)) {
			query.setParameter("displayName", displayName);
		}
		if (notBlank(minCapacityAvailable)) {
			query.setParameter("capacityAvailable", minCapacityAvailable);
		}
		if (notBlank(currentLocationCode)) {
			query.setParameter("currentLocationCode", currentLocationCode);
		}
		if (notBlank(nextLocationCode)) {
			query.setParameter("nextLocationCode", nextLocationCode);
		}
		if (notBlank(status)) {
			query.setParameter("status", status);
		}
		
		List<Container> results = query.getResultList();
		
		logger.info("Matching containers:");
		for (Container container : results) {
			logger.info(container.toString());
		}
		
		return results;
	}
	
	public boolean isShipmentEditable(Long shipmentId) {
		if (shipmentId == null) {
			return true;
		}
		Shipment shipment = entityManager.find(Shipment.class, shipmentId);
		if (shipment == null) {
			return true;
		}
		return shipment.getIsEditable();
	}
	
}
