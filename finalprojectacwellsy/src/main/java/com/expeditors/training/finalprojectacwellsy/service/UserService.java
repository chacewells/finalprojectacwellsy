package com.expeditors.training.finalprojectacwellsy.service;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import com.expeditors.training.finalprojectacwellsy.model.User;

@Repository
public class UserService {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public UserDetails find(String username)
			throws UsernameNotFoundException {
		return entityManager
				.createQuery("Select u from User u where u.username = :username", User.class)
				.setParameter("username", username)
				.getSingleResult();
	}
	
	public Collection<User> listAll() {
		return entityManager.createQuery("Select u from User u", User.class).getResultList();
	}
	
}
