package com.expeditors.training.finalprojectacwellsy.controller;

import static com.expeditors.training.finalprojectacwellsy.dto.FindContainerCriteria.empty;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.expeditors.training.finalprojectacwellsy.dto.FindContainerCriteria;
import com.expeditors.training.finalprojectacwellsy.model.Container;
import com.expeditors.training.finalprojectacwellsy.service.ContainerService;
import com.expeditors.training.finalprojectacwellsy.service.JsService;

@Controller
@RequestMapping("/container/")
public class ContainerController {
	public static final int PAGE_SIZE = 3;
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	@Qualifier("jsContainerService")
	private JsService jsContainerService;
	
	@Autowired
	@Qualifier("containerService")
	private ContainerService containerService;
	
	@Autowired
	private CCHelper ccHelper;
	
	@RequestMapping(method=RequestMethod.GET, value="show.html")
	public ModelAndView showContainer(@RequestParam(value="id") Long id) {
		ModelAndView mav = new ModelAndView("showContainer");
		mav.addObject("container", containerService.getContainer(id));
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="list.html")
	public ModelAndView showAllContainers(@RequestParam(value="page", required=false) Integer page) {
		if (page == null) {page = 0;}
		int start = page*PAGE_SIZE;
		ModelAndView mav = new ModelAndView("listContainers");
		mav.addObject("containers", containerService.listContainers(start, PAGE_SIZE));
		mav.addObject("page", page);
		mav.addObject("morePages", morePages(start));
		mav.addObject("lessPages", lessPages(page));
		mav.addObject("criteria", new FindContainerCriteria());
		mav.addObject("isSearchResult", false);
		
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="list.html")
	public ModelAndView findContainers(
			@ModelAttribute("criteria") FindContainerCriteria criteria,
			@RequestParam(value="page", required=false) Integer page,
			BindingResult result,
			Model m) {
		if (page == null) {page = 0;}
		int start = page*PAGE_SIZE;
		ModelAndView mav = new ModelAndView();
		if (empty(criteria)) {
			mav.setView(new RedirectView("list.html"));
			return mav;
		} else {
			mav.setViewName("listContainers");
		}
		mav.addObject("containers", containerService.findByCriteria( criteria, start, PAGE_SIZE));
		mav.addObject("page", page);
		mav.addObject("morePages", morePages(start, criteria));
		mav.addObject("lessPages", lessPages(page));
		mav.addObject("criteria", criteria);
		mav.addObject("isSearchResult", true);
		
		return mav;
	}

	@RequestMapping(method=RequestMethod.GET, value="add.html")
	public ModelAndView showAddContainer() {
		return showEditContainer(null);
	}

	@RequestMapping(method=RequestMethod.POST, value="add.html")
	public ModelAndView addContainer(
			@ModelAttribute("container") @Valid
			Container container,
			BindingResult result, 
			Model m) {
		ModelAndView mav = new ModelAndView();
		ccHelper.validateJavaContainer(container, result);
		if (result.hasFieldErrors()) {
			mav.setViewName("addContainer");
			mav.addObject("pageAction", pageAction(container));
		} else {
			Long id = containerService.save( container );
			mav.setView(new RedirectView("show.html?id=" + id));
		}
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="edit.html")
	public ModelAndView showEditContainer(@RequestParam(value="id", required=false) Long id) {
		ModelAndView mav = new ModelAndView("addContainer");
		Container container = null;
		if (id != null) {
			container = containerService.getContainer( id );
		}
		if (container == null) {
			container = new Container();
		}
		mav.addObject("container", container);
		mav.addObject("pageAction", pageAction(container));
		return mav;
	}

	@RequestMapping(method=RequestMethod.POST, value="edit.html")
	public ModelAndView editContainer(
			@ModelAttribute("container") @Valid 
			Container container,
			BindingResult result,
			Model m) {
		return addContainer(container, result, m);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="delete.html")
	public ModelAndView deleteContainer(@RequestParam(value="id") Long id) {
		ModelAndView mav = new ModelAndView("deleteContainerConfirm");
		Container container = containerService.getContainer( id );
		boolean isContainerDeleted = containerService.remove( id );
		mav.addObject("isContainerDeleted", isContainerDeleted);
		mav.addObject("container", container);
		return mav;
	}
	
	@RequestMapping(produces=MediaType.APPLICATION_JSON_VALUE,method=RequestMethod.GET, value="get.json")
	@ResponseBody
	public String getContainerJson(@RequestParam("id") Long id) {
		Container container = containerService.getContainer( id );
		return container != null ? container.toJson() : "{}";
	}
	
	private String pageAction(Container container) {
		return 
				container == null || 
				container.getId() == null ||
				containerService.getContainer( container.getId() ) == null ? 
						"Add" :
						"Edit";
	}
	
	private boolean lessPages(int page) {
		return page > 0;
	}
	
	private boolean morePages(int start) {
		return (start + PAGE_SIZE) < containerService.totalContainers();
	}
	
	private boolean morePages(int start, FindContainerCriteria fcc) {
		if (empty(fcc)) {
			return morePages(start);
		} else {
			return (start + PAGE_SIZE) < containerService.totalResults( fcc );
		}
	}
	
}
