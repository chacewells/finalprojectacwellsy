package com.expeditors.training.finalprojectacwellsy.rest;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.expeditors.training.finalprojectacwellsy.model.Container;
import com.expeditors.training.finalprojectacwellsy.service.JsService;

@Component
@Path("/container")
public class ContainerResource {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	
	@Autowired
	JsService containerService;
	
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response createContainer(Container container) {
		Set<ConstraintViolation<Container>> violations = validator.validate(container);
		if (!violations.isEmpty()) {
			return Response.status(400).build();
		}
		
		Long id = containerService.invoke("save", container);
		return Response.created(UriBuilder.fromPath("{id}").build(id)).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response updateContainer(Container container) {
		Set<ConstraintViolation<Container>> violations = validator.validate(container);
		if (!violations.isEmpty()) {
			for (ConstraintViolation<Container> v : violations) {
				logger.info(v.toString());
			}
			return Response.status(400).build();
		}
		
		containerService.invoke("save", container);
		return Response.ok().build();
	}
	
}
