package com.expeditors.training.finalprojectacwellsy.service;

import java.util.List;

import com.expeditors.training.finalprojectacwellsy.dto.FindContainerCriteria;
import com.expeditors.training.finalprojectacwellsy.model.Container;

public interface ContainerService {
	public Container getContainer(Long id);
	public List<Container> listContainers(int start, int max);
	public List<Container> allContainers();
	public List<Container> findByCriteria(FindContainerCriteria criteria, int start, int max);
	public List<Container> findByName(String displayName);
	public int totalResults(FindContainerCriteria criteria);
	public Long totalContainers();
	public Long save(Container container);
	public boolean remove(Long id);
}
