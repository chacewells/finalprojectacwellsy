package com.expeditors.training.finalprojectacwellsy.service;

import javax.script.Invocable;
import javax.script.ScriptException;

public class JsService {
	
	private Invocable jsServiceInvocable;
	
	public JsService(Invocable jsServiceInvocable) {
		this.jsServiceInvocable = jsServiceInvocable;
	}
	
	public <T> T invoke(String function, Object...args) {
		T t = null;
		try {
			t = (T)jsServiceInvocable.invokeFunction(function, args);
		} catch (NoSuchMethodException|ScriptException e) {
			throw new RuntimeException(e);
		}
		return t;
	}
	
}
