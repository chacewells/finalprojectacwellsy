package com.expeditors.training.finalprojectacwellsy.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.expeditors.training.finalprojectacwellsy.model.Container.Status;

public class FreightDataRandomizer {
	final Random rand;
	
	public FreightDataRandomizer(Random rand) {
		this.rand = rand;
	}
	
	public FreightDataRandomizer(long seed) {
		rand = new Random(seed);
	}
	
	public FreightDataRandomizer() {
		rand = new Random();
	}

	public String getCapsString(int length) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < length; ++i) {
			sb.append( (char)(rand.nextInt(26) + 65) );
		}
		
		return sb.toString();
	}
	
	public String getLowerString(int length) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < length; ++i) {
			sb.append( (char)(rand.nextInt(26) + 97) );
		}
		
		return sb.toString();
	}
	
	public String shipmentName() {
		StringBuilder sb = new StringBuilder();
		sb.append(getCapsString(1));
		sb.append(getLowerString(rand.nextInt(25)));
		
		return sb.toString();
	}
	
	public String getNumString(int length) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < length; ++i) {
			sb.append( rand.nextInt(10) );
		}
		
		return sb.toString();
	}
	
	public String getDisplayName() {
		return getCapsString(4) + getNumString(7);
	}
	
	public double getRemainingCapacity() {
		return rand.nextDouble() * 200.;
	}
	
	public String getLocationCode() {
		return getCapsString(3);
	}
	
	public Status getStatus() {
		List<Status> stati = 
				Collections.unmodifiableList(
						Arrays.asList(Status.ARRIVED,
						Status.READY,
						Status.TRANSIT));
		
		return stati.get(rand.nextInt(stati.size()));
	}
	
}
