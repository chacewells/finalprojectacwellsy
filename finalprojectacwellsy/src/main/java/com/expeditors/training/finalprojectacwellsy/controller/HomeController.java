package com.expeditors.training.finalprojectacwellsy.controller;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@Autowired
	ServletContext servletContext;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home() {
		return "redirect:/shipment/list.html";
	}
}
