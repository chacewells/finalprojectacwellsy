package com.expeditors.training.finalprojectacwellsy.dto;

import com.expeditors.training.finalprojectacwellsy.model.Container.Status;

public class FindContainerCriteria {
	private String displayName;
	private Double minCapacityAvailable;
	private String currentLocationCode;
	private String nextLocationCode;
	private Status status;

	public String getCurrentLocationCode() {
		return currentLocationCode;
	}

	public void setCurrentLocationCode(String currentLocationCode) {
		this.currentLocationCode = currentLocationCode;
	}

	public Double getMinCapacityAvailable() {
		return minCapacityAvailable;
	}

	public void setMinCapacityAvailable(Double minCapacityAvailable) {
		this.minCapacityAvailable = minCapacityAvailable;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getNextLocationCode() {
		return nextLocationCode;
	}

	public void setNextLocationCode(String nextLocationCode) {
		this.nextLocationCode = nextLocationCode;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isEmpty() {
		return 
				((displayName == null) || displayName.trim().isEmpty()) &&
				((minCapacityAvailable == null) || minCapacityAvailable <= 0.) &&
				(currentLocationCode == null || currentLocationCode.isEmpty()) &&
				((nextLocationCode == null) || nextLocationCode.isEmpty()) && 
				(status == null);
	}
	
	public static boolean empty(FindContainerCriteria fcc) {
		return fcc == null || fcc.isEmpty();
	}
}
