<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authentication property="principal.username" var="username" />

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<decorator:head />
<title><decorator:title /></title>

<!-- stylesheets -->
<link rel="stylesheet"
	href="<c:url value="/static/css/foundation.min.css" />" />
<link rel="stylesheet" href="<c:url value="/static/css/user.css" />" />

<!-- preloaded scripts -->
<script type="text/javascript"
	src="<c:url value="/static/js/vendor/modernizr.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/static/js/vendor/jquery.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/static/js/foundation.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/static/js/foundation/foundation.topbar.js"/>"></script>
</head>
<body>
	<div class="fixed">
		<nav class="top-bar" data-topbar role="navigation">
			<section class="top-bar-section">
				<ul class="title-area">
					<li><a href="<c:url value="/"/>">Home</a></li>
				</ul>
				<ul class="left">
					<li class="has-form"><a href="<c:url value="/container/list.html"/>">Containers</a></li>
<%-- 					<li class="has-form"><a href="<c:url value="/shipments/list.html"/>">Shipments</a></li> --%>
				</ul>
			</section>
			<section class="top-bar-section">
				<ul class="right">
					<li class="has-form login-info">
						<a>
						<c:choose>
							<c:when test="${not empty username}">
							Logged in as <span class="username">${username}</span>
							</c:when>
							<c:otherwise>Logged out</c:otherwise>
						</c:choose>
						</a>
					</li>
					<c:if test="${not empty username}">
					<li class="has-form">
						<a class="button" href="<c:url value="/logout"/>">Logout</a>
					</li>
					</c:if>
				</ul>
			</section>
		</nav>
	</div>
	<header class="row">
		<div class="row">
			<p class="medium-6 columns">
				<c:choose>
					<c:when test="${not empty username}">
					Logged in as <span class="username">${username}</span>
					</c:when>
					<c:otherwise>Logged out</c:otherwise>
				</c:choose>
			</p>
			<p class="medium-6 medium-text-right columns">
				<c:if test="${not empty username}">
					<a href="<c:url value="/logout"/>">Logout</a>
				</c:if>
			</p>
		</div>
		<div class="row">
			<h1 class="small-text-center">
				<decorator:title />
			</h1>
		</div>
	</header>
	<hr />
	<decorator:body />
	<hr />
	<footer class="row">
		<p class="small-text-center">&copy; 2015 Expeditors Training</p>
	</footer>
</body>
</html>