<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.expeditors.training.finalprojectacwellsy.model.Shipment" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shipments</title>
</head>
<body>
	<table class="columns">
		<tr>
			<th>Name</th>
			<th>Start Location</th>
			<th>End Location</th>
			<th>Volume</th>
			<!-- <th>Status</th> -->
		</tr>
		<c:forEach items="${shipments}" var="shipment">
		<tr>
			<td>${shipment.name}</td>
			<td>${shipment.startLocation}</td>
			<td>${shipment.endLocation}</td>
			<td><fmt:formatNumber type="number" pattern="#.##">${shipment.volume}</fmt:formatNumber></td>
			<%-- <td>${shipment.status}</td> --%>
			<td><a href="<c:url value="/shipment/show.html?id=${shipment.id}"/>">View</a></td>
			<td><c:if test="${shipment.isEditable}"><a href="<c:url value="/shipment/edit.html?id=${shipment.id}"/>">Edit</a></c:if></td>
			<td><input type="hidden" value="${shipment.id}"/><a class="href-remove">Remove</a></td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>