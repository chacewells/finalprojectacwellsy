<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Container Information: ${container.displayName}</title>
		<link rel="stylesheet" 
			  type="text/css"
			  href='<c:url value="/static/css/showContainer.css" />' />
	</head> 
	<body>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Capacity Available</p>
			<p class="propval medium-6 small-only-text-center columns">
				<fmt:formatNumber 
					type="number" 
					pattern="#.##">${container.capacityAvailable}</fmt:formatNumber>
			</p>
		</div>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Current Location</p>
			<p class="propval medium-6 small-only-text-center columns">${container.currentLocationCode}</p>
		</div>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Next Location</p>
			<p class="propval medium-6 small-only-text-center columns">${container.nextLocationCode}</p>
		</div>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Status</p>
			<p class="propval medium-6 small-only-text-center columns">${container.status}</p>
		</div>
		<div class="row">
			<p class="propname small-text-center small-6 small-centered columns">
				<a href="<c:url value="/container/edit.html?id=${container.id}"/>">Edit this Container</a>
			</p>
		</div>
	</body>
</html>
