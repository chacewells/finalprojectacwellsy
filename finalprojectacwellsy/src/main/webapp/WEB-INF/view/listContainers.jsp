<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<title>Containers</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/showAllContainers.css"/>" />
</head>
<body>
	<div class="row">
		<div class="row">
			<div class="medium-6 columns"><h5><a href="<c:url value="/container/add.html"/>">Add a Container</a></h5></div>
			<div class="medium-6 columns">
				<div class="row"><div class="small-10 columns"><a id="search-bar-toggle">Search</a></div></div>
				<form:form method="POST" action="list.html" commandName="criteria">
					<label for="currentLocationCode">Current Location:</label>
					<form:input path="currentLocationCode" placeholder="e.g. 'PDX'"/>
					<label for="displayName">Display Name:</label>
					<form:input path="displayName" placeholder="e.g. 'XXXX1234567'"/>
					<input type="hidden" id="page" name="page" value="0"/>
					<input class="button" type="submit" value="Find"/>
				</form:form>
			</div>
		</div>
		<div class="row">
			<div class="small-6 columns">
			<c:if test="${lessPages}">
				<c:choose>
					<c:when test="${isSearchResult}"><a id="previous">Previous</a></c:when>
					<c:otherwise><a href="list.html?page=${page - 1}">Previous</a></c:otherwise>
				</c:choose>
			</c:if>
			</div>
			<div class="small-6 small-text-right columns">
			<c:if test="${morePages}">
				<c:choose>
					<c:when test="${isSearchResult}"><a id="next">Next</a></c:when>
					<c:otherwise><a href="list.html?page=${page + 1}">Next</a></c:otherwise>
				</c:choose>
			</c:if>
			</div>
		</div>
		<table class="columns">
			<tr>
				<th>Display Name</th>
				<th>Capacity Available</th>
				<th>Current Location Code</th>
				<th>Next Location Code</th>
				<th>Status</th>
			</tr>
			<c:forEach items="${containers}" var="container">
			<tr>
				<td>${container.displayName}</td>
				<td><fmt:formatNumber type="number" pattern="#.##">${container.capacityAvailable}</fmt:formatNumber></td>
				<td>${container.currentLocationCode}</td>
				<td>${container.nextLocationCode}</td>
				<td>${container.status}</td>
				<td><a href="<c:url value="/container/show.html?id=${container.id}"/>">View</a></td>
				<td><a href="<c:url value="/container/edit.html?id=${container.id}"/>">Edit</a></td>
				<td><input type="hidden" value="${container.id}"/><a class="href-remove">Remove</a></td>
			</tr>
			</c:forEach>
		</table>
	</div>
	<script type="text/javascript">
	$(document).ready(function () {
		$('a.href-remove').click(function () {
			console.log('a.href-remove clicked');
			var id = $(this).siblings('input[type="hidden"]')[0].value;
			$.getJSON("get.json?id=" + id, function (container) {
				var confirmBody = "Are you sure you want to remove this container?\n\
Display Name: " + container.displayName + "\n\
Capacity Available: " + container.capacityAvailable + "\n\
Current Location: " + container.currentLocationCode + "\n\
Next Location: " + container.nextLocationCode + "\n\
Status: " + container.status;
				if ( !container.hasOwnProperty('displayName') ) {
					window.alert("Sorry, that Container doesn't exist");
				} else if (window.confirm(confirmBody)) {
					window.location = 'delete.html?id=' + container.id;
				}
			});
		});
		
		$('a#next').click(function () {
			$('input#page').val('${page+1}');
			$('form#criteria').trigger('submit');
		});
		
		$('a#previous').click(function () {
			$('input#page').val('${page-1}');
			$('form#criteria').trigger('submit');
		});
		
		<c:if test="${!isSearchResult}">
		$('form#criteria').hide();
		</c:if>
		$('a#search-bar-toggle').click(function () {
			$('form#criteria').toggle('1s');
		});
	});
	</script>
</body>
</html>