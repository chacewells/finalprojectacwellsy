<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Shipment for <sec:authentication property="principal.username"/></title>
</head>
<body>
<form:form method="POST" commandName="shipment">
	<div class="container">
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Name:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="name" />
			</p>
			<p class="medium-3 columns"><form:errors path="name" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Start Location:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="startLocation" />
			</p>
			<p class="medium-3 columns"><form:errors path="startLocation" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">End Location:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="endLocation" />
			</p>
			<p class="medium-3 columns"><form:errors path="endLocation" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Volume:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="volume" />
			</p>
			<p class="medium-3 columns"><form:errors path="volume" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="small-3 small-centered columns">
				<input type="submit" class="button" value="Add Shipment" />
			</p>
		</div>
	</div>
</form:form>
</body>
</html>