<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>That ship has sailed!</title>
</head>
<body>
<div class="small-centered row">
	<h3 class="small-text-center columns">${shipment.name}</h3>
	<p class="small-text-center columns">You can't edit a shipment after it has shipped.</p>
</div>
</body>
</html>