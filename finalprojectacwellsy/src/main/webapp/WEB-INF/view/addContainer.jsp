<%@page import="javax.persistence.metamodel.SetAttribute"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.expeditors.training.finalprojectacwellsy.model.Container" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${pageAction} Container</title>
</head>
<body>
<form:form method="POST" commandName="container">
	<div class="container">
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Display Name:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="displayName" />
			</p>
			<p class="medium-3 columns"><form:errors path="displayName" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Capacity Available:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="capacityAvailable" />
			</p>
			<p class="medium-3 columns"><form:errors path="capacityAvailable" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Current Location Code:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="currentLocationCode" />
			</p>
			<p class="medium-3 columns"><form:errors path="currentLocationCode" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Next Location Code:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="nextLocationCode" />
			</p>
			<p class="medium-3 columns"><form:errors path="nextLocationCode" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Status:</p>
				<div class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:select path="status"><form:options/></form:select>
				</div>
			<p class="medium-3 columns"><form:errors path="status" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="small-3 small-centered small-text-center columns">
				<input type="submit" class="button" value="Done" />
			</p>
		</div>
	</div>
</form:form>
</body>
</html>