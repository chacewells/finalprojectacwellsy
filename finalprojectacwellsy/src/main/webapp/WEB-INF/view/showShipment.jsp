<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Shipment Information: ${shipment.name}</title>
		<link rel="stylesheet" 
			  type="text/css"
			  href='<c:url value="/static/css/showContainer.css" />' />
	</head> 
	<body>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Start Location</p>
			<p class="propval medium-6 small-only-text-center columns">
				${shipment.startLocation}
			</p>
		</div>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">End Location</p>
			<p class="propval medium-6 small-only-text-center columns">${shipment.endLocation}</p>
		</div>
		<div class="row">
			<p class="propname small-only-text-center medium-text-right medium-6 columns">Volume</p>
			<p class="propval medium-6 small-only-text-center columns">
			<fmt:formatNumber pattern="#.##">${shipment.volume}</fmt:formatNumber>
			</p>
		</div>
		<div class="row">
			<h5 class="propname small-text-center small-12 columns">Dispatched Containers</h5>
			<c:forEach items="${shipment.assignedContainers}" var="container">
			<p class="small-text-center small-12 columns">${container.displayName}</p>
			</c:forEach>
		</div>
	</body>
</html>
