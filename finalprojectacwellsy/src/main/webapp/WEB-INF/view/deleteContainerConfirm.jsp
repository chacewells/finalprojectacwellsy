<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>
<c:choose>
	<c:when test="${isContainerDeleted}">Container Removed</c:when>
	<c:otherwise>Nothing to Remove</c:otherwise>
</c:choose>
</title>
</head>
<body>
<p>
<c:choose>
<c:when test="${isContainerDeleted}">
This container was successfully removed.
</c:when>
<c:otherwise>
No container was found for that id.
</c:otherwise>
</c:choose>
</p>
</body>
</html>