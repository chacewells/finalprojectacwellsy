<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
    pageEncoding="ISO-8859-15"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Registered Users</title>
</head>
<body>
<c:forEach items="${users}" var="user">
	<p>
	Username: ${user.username}<br/>
	Password: ${user.password}<br/>
	Roles: <c:forEach items="${user.roles}" var="role">|${role.userRole}|</c:forEach>
	</p>
</c:forEach>
</body>
</html>